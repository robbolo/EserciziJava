public class TipiDiBase {
    public static void main (String[] args) {
        
        int i = 3;
        int iMin = Integer.MIN_VALUE;
        int iMax = Integer.MAX_VALUE;
        
        System.out.println(i);
        System.out.println();
        
        System.out.println();
        System.out.println("Int"); 
        System.out.println(iMax);
        System.out.println(iMin);

        System.out.println();
        System.out.println("Byte");
        System.out.println(Byte.MAX_VALUE);
        System.out.println(Byte.MIN_VALUE);

        System.out.println();
        System.out.println("Short");
        System.out.println(Short.MIN_VALUE);
        System.out.println(Short.MIN_VALUE);

        System.out.println();
        System.out.println("Long");
        System.out.println(Long.MIN_VALUE);
        System.out.println(Long.MIN_VALUE);

        // Studio Overflow

        System.out.println("Overflow");
        System.out.println(iMax+1);

        // Overflow ciclico
        System.out.println();
        System.out.println("Overflow ciclico");
        int overCic = iMax - 100000000;
        boolean cicla = true;
        while (cicla) { // Niente nel corpo del while può modificare questo predicato --> Diventerà un Loop SE non inserisco qualcosa prima tipo una variabile booleana
                System.out.println();
                System.out.println(overCic);
                overCic = overCic + 1000;
        }

        System.out.println();
        System.out.println("Dopo il Loop");
    }   
}