public class B {
    public static void main (String[] args) {
        boolean v = true;
        boolean f = false;

        boolean b0 = (1 == 1);
        System.out.println("Uguaglianza");
        System.out.println(b0);
        boolean b1 = (2 == 1);
        System.out.println("Diseguaglianza");
        System.out.println(b0);
        b1 = (2 != 1);
        System.out.println(b1);
        System.out.println("Diseguaglianza numero 2 ");
        System.out.println(b1);
        boolean b2 = ( 1 >= 1);
        System.out.println("Maggiore uguale");
        System.out.println(b2);
        b2 = ( 2>= 1 );
        System.out.println("Maggiore uguale numero 2");
        System.out.println(b2);
        // 1 < 3 e 3 <= 25 ?
        boolean b3 = ((1 < 3 ) && (3 <= 25));
        System.out.println("operatore di congiunzione :  AND");
        System.out.println(b3);
        
        // TAVOLA DI VERITA della CONGIUNZIONE:

        System.out.println();
        System.out.println("TAVOLA DI VERITA");
        System.out.println(f && f);
        System.out.println(f && v);
        System.out.println(v && f);
        System.out.println(v && v);

        // TAVOLA DI VERITA della DISGIUNZIONE:
        System.out.println("TAVOLA DI VERITA || DISGIUNZIONE");
        System.out.println(f || f);
        System.out.println(f || v);
        System.out.println(v || f);
        System.out.println(v || v);
        
        // TAVOLA DI VERITA della NEGAZIONE:
        System.out.println("TAVOLA DI VERITA ! NEGAZIONE");
        System.out.println(!f);
        System.out.println(!v);

        // Negazione di espressione complessa

        System.out.println("Negazione di espressioni complicate");
        System.out.println(
            !(1 < 3) && !(25 > 3)
        );
        
        //Legge di De Morgan 
        
        System.out.println("Legge di De Morgan");
        System.out.println(
        !((1 > 3) || (25 >3))
        );
        
        //Lazyness di && e ||
        
        System.out.println("Lazyness di && e ||");
        // Questo darebbe un errore poichè intero / 0 è impossibile : int errore = 3/0;
        System.out.println(
            (1 < 3) || (3/0 == 1)       //Essendo il primo blocco 1<3 vero, il compilatore non legge il secondo blocco e restituisce subito vero, anche se nel secondo blocco c'è roba impossibile
        );
        
       

        
    }
}