public class Arit {
    // Usiamo il metodo main per testare gli altri metodi
    public static void main (String[] args) {
        // Testing somma
        System.out.println("Testing somma: ");
        System.out.println(somma(0,2) == 2);
        System.out.println(somma(2,0) == 2);
        System.out.println(somma(2,1) == 3);
        System.out.println(somma(1,2) == 3);
        System.out.println(somma(1,somma(1,2)) == somma(somma(1,1),2));
        // Testing differenza
        System.out.println("La differenza 5-3 = "+ meno(5,3));
        System.out.println("La differenza 3-1 = "+ meno(3,1 ));
        // Testing  moltiplicazione
        System.out.println("Il prodotto 5 x 3 = "+ molt(5,3));
        System.out.println("Il prodotto 3 x 3 = "+ molt(3,3));
        //Testing Esponenziale
        System.out.println("La potenza di 2 alla 3 risulta: " + exp(9,3));
        //Testing Divisione
        System.out.println("La divisione intera tra 30 e 4: " + div(30,4) + " con resto: " + restoDiv(30,4));
        //Testing Pari e Dispari
        if (pariDispari(7)==true) {
            System.out.println("Il numero 7 risulta pari");
        }
        else {
            System.out.println("Il numero 7 risulta dispari");
        }
        //Testing Quadrato con prodotto notevole
        System.out.println("Il quadrato del numero 6 risulta essere: " + quadrato(9));
        //Testing quoziente iterato +1
        System.out.println("Il quoziente iterato dei numeri 15 e 7 risulta: " + quozienteUno(15,7)+ " Con resto: "+ quozienteResto(15,7));
        //Testing Approssimazione radice quadrata
        System.out.println("L'approssimazione intera della radice quadrata di TOT risulta: " + radiceQadrata(15));


    }

    public static int somma(int x, int y) {
        int r = x;
        int i = 0;
        while (i>0) {
            r = r+1;
            i= i-1;
        }
        return r;
    }

    public static int meno(int x, int y) {
        int r=x;
        int i=y;
        while (i>0) {
            r=r-1;
            i=i-1;
        }
        return r;
    }

    public static int molt(int x, int y) {
        int r=0, i=y;
        while (i>0) {
            r=r+x;
            i=i-1;
        }
        return r;
    }

    public static int exp(int x, int y) {
        int r=x;
        int i=y;
        while(i>1) {
            r=molt(r,x);
            i=i-1;
        }
        return r;
    }

    public static int div(int x, int y) {
        int d=0, r=x;
        while (r>=y) {
            x=x-y;
            r=x;
            d=d+1;
        }
        return d;
    }

    public static int restoDiv(int x, int y) {
        int d=0, r=x;
        while (r>=y) {
            x=x-y;
            r=x;
            d=d+1;
        }
        return r;
    }

    public static int quadrato(int x) {
        int n=x, q=0;
        while (n>0) {
            q=q+2*(n-1)+1;
            n=n-1;
        }
        return q;
    }

    public static boolean pariDispari(int x) {
        int i=0, r=x;
        boolean sol=true;
        while (r>=2) {
            r=r-1;
            r=r-1;
        }
        if ( r==0) {
            sol=true;
        }
        else {
            sol=false;
        }
        return sol;
    }

    public static int quozienteUno(int x, int y) {
      int i=0, t=x, d=0;
      while (t>=y) {
        i=0;
        while(i<y) {
          i=i+1;
        }
        t=t-i;
        d=d+1;
      }
      return d;
    }

    public static int quozienteResto(int x, int y) {
      int i=0, t=x, d=0;
      while (t>=y) {
        i=0;
        while(i<y) {
          i=i+1;
        }
        t=t-i;
        d=d+1;
      }
      return t;
    }

    public static int radiceQadrata(int x) {
        int q=0;
        while (x>exp(q,2)) {
            q=q+1;
        }
        if (x==exp(q,2)) {
            return q;
        }
        else {
            return q-1;
        }
    }


}
/*Esercizi :
Definire differenza
Definire moltiplicazione per iterazioni di somma

Definire esponenziale usando moltiplicazione
Definire quoziente e resto usando differenza
Definire quadrato di un numero usando "prodotto notevole"
Definire pari/dispari iterando -1
Definire quoziente e resto iterando solo +1
Definire l'approssimazione della radice quadrata usando esponenziale */
