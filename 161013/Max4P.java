public class Max4P {
    
    public static void main (String[] args) { //metodo main : punto d'ingresso
        // avendo (m,n,p,q), in questa fase ho(?,?,?,?) configurazione iniziale
        int m = 3;
        int n = 8;
        int p = 2;
        int q = 1;
        int t = Integer.MAX_VALUE;
        // (3,8,2,1) effettiva configurazione iniziale
        if (m > n) { // confronta m > n cioè 3 > 8
                t = n;
                n = m;
                m = t;
        }       
        // qua inseriremo dei comandi per interagire con lo standard output (schermo)
        // (3,8,2,1) effettiva configurazione dopo la selezione
        System.out.println(
            "(" + m + "," + n + "," + p + "," + q + ")"
        );
        System.out.println(t); //sto stampando la variabile t definita come il massimo numero intero, per vederne il valore
    }
}