public class Filtri {
    public static void soloDispari (int n) {
        int i = 0;
        System.out.println("Hai scelto di dover inserire " + n + " numeri.");
        while (i < n) {
            System.out.println("\nInserisci il numero :" + (i+1));
            int x = SIn.readInt();
            if (!Arit.pariDispari(x)) {
                System.out.println(x + " e' dispari");
            } else {
                System.out.println(x+ " e' pari");
            }
            i=i+1;
        }
        System.out.println("\nHai concluso l'inserimento.");
    }

    public static void sopraSoglia (int n , int s) {
        int i=0, soglia=s;
        System.out.println("Hai scelto di dover inserire " + n + " numeri con soglia "+ soglia);
        //perogni 0<=k<0 && i==0 se (x>soglia)-->(stampa(x))
        while (i < n) {
            //perogni 0<=k<i && i<=n se (x>soglia)-->(stampa(x))
            System.out.println("\nInserisci il numero :" + (i+1));
            int x = SIn.readInt();
            if (x>soglia) {
                System.out.print("---> "+x + " e' maggiore della soglia "+soglia);
            } else {
                System.out.print("---> "+x+ " non e' maggiore della soglia "+soglia);
            }
            //perogni 0<=k<=i && i<=n se (x>soglia) -->(stampa(x))
            i=i+1;
        //perogni 0<=k<i && i<=n se (x>soglia) --> (stampa(x)
        }
        System.out.println("\nHai concluso l'inserimento.");
        //perogni 0<=k<=i && i==n se (x>soglia)-->(stampa(x))
    }


        public static void doppioSoglia (int n , int s) {
        int i=0, soglia=s;
        System.out.println("Hai scelto di dover inserire " + n + " numeri con soglia "+ soglia);
        //perogni 0<=k<1 && i==0 se (x==2*soglia)-->(stampa(x))
        while (i < n) {
            //perogni 0<=k<i && i<n se (x==2*soglia)-->(stampa(x))
            System.out.println("\nInserisci il numero :" + (i+1));
            int x = SIn.readInt();
            if (x == 2*soglia) {
                System.out.print("---> "+x + " e' uguale al doppio della soglia "+soglia);
            } else {
                System.out.print("---> "+x+ " non e' uguale al doppio della soglia "+soglia);
            }
            //perogni k di R, con k: 0<=k<=i && i<=n se (x==2*soglia)-->(stampa(x))
            i=i+1;
        //perogni 0<=k<i && i<=n se (x==2*soglia) --> (stampa(x))
        }
        System.out.println("\nHai concluso l'inserimento.");
        //perogni k di R, con k: 0<=k<=i && i==n se (x==2*soglia)-->(stampa(x))
    }


    public static void limitiEsterniSoglia (int n , int s1, int s2) {
        int i=0, sogliaMax=s1, sogliaMin=s2;
        System.out.println("Hai scelto di dover inserire " + n + " numeri con soglia max "+ sogliaMax+" e soglia min "+sogliaMin);
        //perogni 0<=k<1 && i==0 se (x==2*soglia)-->(stampa(x))
        while (i < n) {
            //perogni 0<=k<i && i<n se (x==2*soglia)-->(stampa(x))
            System.out.println("\nInserisci il numero :" + (i+1));
            int x = SIn.readInt();
            if (x < sogliaMin || x > sogliaMax) {
                System.out.print("---> "+x + " e' minore di "+sogliaMin+" oppure maggiore di "+sogliaMax);
            }else {
                System.out.print("---> "+x + " non e' minore di "+sogliaMin+" oppure maggiore di "+sogliaMax);
            }
            //perogni k di R, con k: 0<=k<=i && i<=n se (x==2*soglia)-->(stampa(x))
            i=i+1;
        //perogni 0<=k<i && i<=n se (x==2*soglia) --> (stampa(x))
        }
        System.out.println("\nHai concluso l'inserimento.");
        //perogni k di R, con k: 0<=k<=i && i==n se (x==2*soglia)-->(stampa(x))
    }

    public static void limitiInterniSoglia (int n , int s1, int s2) {
        int i=0, sogliaMax=s1, sogliaMin=s2;
        System.out.println("Hai scelto di dover inserire " + n + " numeri con soglia max "+ sogliaMax+" e soglia min "+sogliaMin);
        //perogni 0<=k<1 && i==0 se (x==2*soglia)-->(stampa(x))
        while (i < n) {
            //perogni 0<=k<i && i<n se (x==2*soglia)-->(stampa(x))
            System.out.println("\nInserisci il numero :" + (i+1));
            int x = SIn.readInt();
            if (x > sogliaMin && x < sogliaMax) {
                System.out.print("---> "+x + " e' compreso tra "+sogliaMin+" e "+sogliaMax);
            }else {
                System.out.print("---> "+x + " non e' compreso tra "+sogliaMin+" e "+sogliaMax);
            }
            //perogni k di R, con k: 0<=k<=i && i<=n se (x==2*soglia)-->(stampa(x))
            i=i+1;
        //perogni 0<=k<i && i<=n se (x==2*soglia) --> (stampa(x))
        }
        System.out.println("\nHai concluso l'inserimento.");
        //perogni k di R, con k: 0<=k<=i && i==n se (x==2*soglia)-->(stampa(x))
    }

    public static void media (int n ) {
      int accumulo = 0, i = 0, valore = 0;
      while (i < n) {
        System.out.println("\nInserisci il numero: " + (i+1));
        valore = SIn.readInt();
        accumulo += valore;
        i++;
      }
      System.out.println("\nLa media dei valori inseriti risulta: "+ (float) (accumulo)/n+" con resto: "+ accumulo%n);
    }
    //per ogni k di R, 0<=k<i && i==n && accumulo==Sommatoria(valore(k-esimo)) && (r==Sommatoria(valore(k-esimo))/n)
}
