public class Test {
  public static void main (String[] args) {
    int n;
    int scelta = 10;
    while (scelta!=0) {
      System.out.println("\nMenu di scelta: \n1: Somma+differenza+prodotto+divisione iterata+potenza in test.\n2: StampaDIspari.\n3:Soprasoglia.\n4:Doppiosoglia.\n5:Intervalloesterno.\n6:IntervalloInterno\n7:Media.\n0:fine.");
      scelta = SIn.readInt();
      if(scelta == 1) {
        System.out.println("Inserire primo numero: ");
        int num1 = SIn.readInt();
        System.out.println("Inserire il secondo numero: ");
        int num2 = SIn.readInt();
        System.out.println("La somma di numero1 e numero2 risulta: " + Arit.somma(num1,num2));
        System.out.println("La differenza di 5 e 2 risulta: " + Arit.meno(5,2));
        System.out.println("Il prodotto tra 3 e 2 risulta: " + Arit.molt(3,2));
        System.out.println("La divisione  iterata tra x e y risulta essere: " + Arit.quozienteUno(num1,num2));
        System.out.println("La potenza 4 alla 2 risulta : " + Arit.exp(4,2));
      }else if (scelta == 2) {
        System.out.println("StampaDispari: Inserisci il numero di interi da analizzare e stamparne i dispari: ");
        n = SIn.readInt();
        Filtri.soloDispari(n);
      }else if (scelta == 3) {
        System.out.println("SopraSoglia: Inserisci il numero di interi da analizzare: ");
        n = SIn.readInt();
        System.out.println("Inserisci la soglia per analizzare la sequenza precedente");
        int sogliaTest = SIn.readInt();
        Filtri.sopraSoglia(n,sogliaTest);
      }else if (scelta == 4) {
        System.out.println("DoppioSoglia: Inserisci il numero di interi da analizzare: ");
        n = SIn.readInt();
        System.out.println("Inserisci la soglia da raddoppiare per analizzare la sequenza precedente");
        int sogliaTest = SIn.readInt();
        Filtri.doppioSoglia(n,sogliaTest);
      }else if (scelta == 5) {
        System.out.println("LimitiSoglia: Inserisci il numero di elementi da analizzare: ");
        n = SIn.readInt();
        System.out.println("Inserisci la soglia inferiore: ");
        int sogliaMin = SIn.readInt();
        System.out.println("Inserisci la soglia superiore: ");
        int sogliaMax = SIn.readInt();
        Filtri.limitiEsterniSoglia(n,sogliaMax,sogliaMin);
      }else if (scelta == 6) {
        System.out.println("LimitiSoglia: Inserisci il numero di elementi da analizzare: ");
        n = SIn.readInt();
        System.out.println("Inserisci la soglia inferiore: ");
        int sogliaMin = SIn.readInt();
        System.out.println("Inserisci la soglia superiore: ");
        int sogliaMax = SIn.readInt();
        Filtri.limitiInterniSoglia(n,sogliaMax,sogliaMin);
      }else if (scelta == 7) {
        System.out.println("Media: Inserisci il numero di elementi di cui vuoi conoscere la media: ");
        n = SIn.readInt();
        Filtri.media(n);
      }
    }
  }
}
