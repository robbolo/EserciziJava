public class TestAritRic {
    public static void main (String[] args) {
        int scelta = 0;
        while (scelta != 10) {
            System.out.println("\nInserisci: \n 0 per Somma ricorsiva \n 1 per Differenza Ricorsiva \n 2 per Prodotto Ricorsivo \n 3 per Divisione Ricorsiva \n 4 per Fattoriale Ricorsivo \n 10 per chiudere");
            System.out.print("-->");
            scelta = SIn.readInt();
            if ( scelta == 0) {    
                // Test sommaRic
                System.out.println("\nInserisci primo numero da sommare: ");
                int a = SIn.readInt();
                System.out.println("Inserisci secondo numero da sommare: ");
                int b = SIn.readInt();
                System.out.println("Il risultato della somma col metodo ricorsivo risulta: " + AritRic.sommaRic(a,b));
            }else if (scelta == 1) {
                // Test DiffRic
                System.out.println("\nInserisci il primo numero per la sottrazione: ");
                int a = SIn.readInt();
                System.out.println("Inserisci il secondo numero per la sottrazione: ");
                int b = SIn.readInt();
                System.out.println("il risultato della differenze ricorsiva risulta essere: " + AritRic.diffRic(a,b));
            }else if (scelta == 10) {
                System.out.println("\nGrazie, hai concluso!");
            }else if (scelta == 2) {
                 // Test ProdottoRic
                System.out.println("\nInserisci il primo numero per il prodotto ricorsivo: ");
                int a = SIn.readInt();
                System.out.println("Inserisci il secondo numero per il prodotto ricorsivo: ");
                int b = SIn.readInt();
                System.out.println("il risultato del prodotto ricorsivo risulta essere: " + AritRic.prodRic(a,b));
            }else if (scelta == 3) {
                // Test RestoRic
                System.out.println("\nInserisci il dividendo per la divisione ricorsiva: ");
                int a = SIn.readInt();
                System.out.println("Inserisci il divisore per la divisione ricorsiva: ");
                int b = SIn.readInt();
                System.out.print("Il risultato della divisione ricorsiva intera risulta essere: " + AritRic.quozRic(a,b));
                System.out.println(" con resto: " + AritRic.rRic(a,b));
            }else if (scelta == 4) {
                //Test FattorialeRIc
                System.out.println("\nInserisci il numero di cui calcolare il fattoriale ricorsivo: ");
                int a = SIn.readInt();
                System.out.println("Il fattoriale calcolato per "+ a+ " risulta essere: "+AritRic.fattRic(a));
            }else 
                System.out.println("Valore non riconosciuto");
        }
    }
}
