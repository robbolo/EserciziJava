public class AritRic {
    
    public static int sommaRic (int x, int y) {
        int a = x, b = y;
        if ( b == 0 ) 
            return a; 
        else 
            return (1 + sommaRic(a, b-1));
    }
    
    public static int diffRic (int x, int y) {
        int a = x, b = y;
        if (b == 0)
            return a;
        else
            return (diffRic(a, b-1) - 1);
    }
    
    public static int prodRic ( int x, int y) {
        int a = x, b = y;
        if ( b == 1)
            return a;
        else 
            return (prodRic(a, b-1) + a);
    }

    public static int quozRic (int x, int y) {
        int a = x, b = y;
        if (a<b)
            return 0;
        else 
            return (quozRic(a-b,b) +1); 
    }
    
    public static int rRic (int x, int y) {
        int a = x, b = y;
        if (a<b)
            return a;
        else 
            return (rRic(a-b,b)); 
    }

    public static int fattRic (int x) {
        int a = x;
        if ( a == 0)
            return 1;
        else    
            return (fattRic(a-1)*a);
    }
}